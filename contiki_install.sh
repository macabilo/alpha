#!/bin/bash

set -eu -o pipefail #fail on error and report it, debug all lines

sudo -n true
test $? -eq 0 || exit 1 "You should have sudo privileges to run this script"

#Update package repository in case
echo "Checking for updates..."
sudo apt-get update -y --allow-releaseinfo-change && sudo apt-get upgrade -y

echo "Installing required packages..."
sudo apt-get install build-essential doxygen curl python3-pip -y
echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt-get install wireshark -y
if ! [ $(getent group wireshark) ]; then
	sudo groupadd wireshark
fi
sudo usermod -a -G wireshark pi

pip install pyserial

sudo apt-get install gcc-msp430 gcc-arm-none-eabi gdb-arm-none-eabi srecord default-jdk ant -y
sudo apt-get install libncursesw5-dev libgdbm-dev libc6-dev zlib1g-dev libsqlite3-dev tk-dev libssl-dev openssl libncurses5-dev bridge-utils -y
sudo apt-get install gcc flex bison libboost-graph-dev binutils-msp430 msp430-libc msp430mcu mspdebug binutils-avr gcc-avr gdb-avr avr-libc avrdude -y
#sudo apt-get install tinyos-tools -y #package non-existing (last checked April 2022)

echo 'export JAVA_HOME="/usr/lib/jvm/default-java" ' > ~/.profile
sudo usermod -a -G plugdev pi
sudo usermod -a -G dialout pi

echo "Installing contiki-ng..."
cd ~
if ! [ -d "cnextgen" ]; then
	git clone https://github.com/contiki-ng/contiki-ng.git cnextgen
	cd cnextgen
	git submodule update --init --recursive

	echo "iot-workshop branch is available as a WORKING COPY of sample codes"

else
	echo "Contiki-NG already exists"
fi

cd ~
echo "Installing contiki-3.0..."
if ! [ -d "contiki-3.0" ]; then
	git clone https://github.com/contiki-os/contiki.git contiki-3.0
	cd contiki-3.0
	git submodule sync && git submodule update --init
	git pull

	cd ~
    echo "Pulling iot-workshop sample codes..."
    cd contiki-3.0
    git remote add iot-workshop https://github.com/alignan/contiki
	git fetch iot-workshop
	git checkout iot-workshop
else
	echo "Contiki 3.0 already installed"
fi

cd ~
echo "Cloning macabilo/contiki-projects ..."
cd contiki-3.0
if ! [ -d "contiki-projects" ]; then
	git clone https://gitlab.com/macabilo/contiki-projects
	cd contiki-projects
	git pull
	echo "Contiki projects folder cloned successfully"
else
	echo "Contiki projects folder already existed"
fi


echo ""
echo "Done."
