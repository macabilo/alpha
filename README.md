# SenseDev Installation Guide
Author: M.A. Cabilo
Date: July 2019

A script for installing contiki-3.0 and contiki-ng in raspberry pi or any linux environment



# Step 1
Run  **contiki installer script** (contiki_install.sh)

# Step 2
Run **database installer script** (database_install.sh)

During this installation, you will be prompted several times (sometimes between long time intervals).
Fill in the following details:  
a) When asked about the type of web server to run phpmyadmin, select **apache2**  
b) When asked if your phpmyadmin will be configured with dbconfig-common, select **yes**  
c) Next, you will be asked for a password for phpmyadmin default account. You'll repeat this password for confirmation  

Finally, you should see an "ok" status after apache2 has been restarted by the script.
At this point, your database is ready.

